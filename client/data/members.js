const members = [
  {
    "code": 0,
    "gender": "male",
    "name": "mr asd qwe",
    "email": "asd.qwe@gmail.com",
    "phone": "(179)-102-8139",
    "cell": "(107)-396-0688",
    "picture": "https://randomuser.me/api/portraits/men/8.jpg"
  },
  {
    "code": 1,
    "gender": "male",
    "name": "mr samuel ross",
    "email": "samuel.ross@example.com",
    "phone": "(179)-102-8139",
    "cell": "(107)-396-0688",
    "picture": "https://randomuser.me/api/portraits/men/80.jpg"
  },
  {
    "code": 2,
    "gender": "female",
    "name": "miss iiris salmi",
    "email": "iiris.salmi@example.com",
    "phone": "03-210-038",
    "cell": "041-248-90-32",
    "picture": "https://randomuser.me/api/portraits/women/20.jpg"
  },
  {
    "code": 3,
    "gender": "female",
    "name": "miss katie freeman",
    "email": "katie.freeman@example.com",
    "phone": "03-9369-0971",
    "cell": "0492-463-362",
    "picture": "https://randomuser.me/api/portraits/women/33.jpg"
  },
  {
    "code": 4,
    "gender": "male",
    "name": "monsieur baptiste roger",
    "email": "baptiste.roger@example.com",
    "phone": "(433)-049-9483",
    "cell": "(613)-373-7079",
    "picture": "https://randomuser.me/api/portraits/men/18.jpg"
  },
  {
    "code": 5,
    "gender": "female",
    "name": "ms susana ibañez",
    "email": "susana.ibañez@example.com",
    "phone": "972-188-835",
    "cell": "649-007-734",
    "picture": "https://randomuser.me/api/portraits/women/17.jpg"
  },
  {
    "code": 6,
    "gender": "male",
    "name": "mr max smith",
    "email": "max.smith@example.com",
    "phone": "(511)-782-9997",
    "cell": "(839)-200-1370",
    "picture": "https://randomuser.me/api/portraits/men/5.jpg"
  },
  {
    "code": 7,
    "gender": "female",
    "name": "miss فاطمه گلشن",
    "email": "فاطمه.گلشن@example.com",
    "phone": "057-89321139",
    "cell": "0905-454-0800",
    "picture": "https://randomuser.me/api/portraits/women/84.jpg"
  },
  {
    "code": 8,
    "gender": "female",
    "name": "ms vancléia alves",
    "email": "vancléia.alves@example.com",
    "phone": "(22) 4340-5667",
    "cell": "(67) 8164-8749",
    "picture": "https://randomuser.me/api/portraits/women/45.jpg"
  },
  {
    "code": 9,
    "gender": "male",
    "name": "mr aaron reichert",
    "email": "aaron.reichert@example.com",
    "phone": "0877-1562096",
    "cell": "0178-7366696",
    "picture": "https://randomuser.me/api/portraits/men/91.jpg"
  },
  {
    "code": 10,
    "gender": "female",
    "name": "mrs enni lampo",
    "email": "enni.lampo@example.com",
    "phone": "06-327-126",
    "cell": "046-623-83-58",
    "picture": "https://randomuser.me/api/portraits/women/47.jpg"
  },
  {
    "code": 11,
    "gender": "female",
    "name": "mrs sophie graves",
    "email": "sophie.graves@example.com",
    "phone": "09-3659-3165",
    "cell": "0473-363-818",
    "picture": "https://randomuser.me/api/portraits/women/57.jpg"
  }
];

export default members;
