export function addMember(serial, gender, name, email, phone, cell, picture) {
  return {
    type: 'ADD_MEMBER',
    code,
    gender,
    name,
    email,
    phone,
    cell,
    picture
  }
}

export function removeMember(i) {
  return {
    type: 'REMOVE_MEMBER',
    i
  }
}
