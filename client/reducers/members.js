export default function comments(state = [], action) {
  switch(action.type) {
    case 'ADD_MEMBER':
      return [...state, {
        code: action.code,
        gender: action.gender,
        name: action.name,
        email: action.email,
        phone: action.phone,
        cell: action.cell,
        picture: action.picture
      }];
    case 'REMOVE_MEMBER':
      return [
        ...state.slice(0, action.i),
        ...state.slice(action.i + 1)
      ];
    default:
      return state;
  }
}
