import React from 'react';
import { render } from 'react-dom';
import { Router, Route, IndexRoute, browserHistory } from 'react-router';
import { Provider } from 'react-redux';

import css from './styles/style.less';

import App from './components/App';
import Member from './components/Member';
import MemberList from './components/MemberList';

import store, { history } from './store';

const route = (
  <Provider store={store}>
    <Router history={history}>
      <Route path="/" component={App}>
        <IndexRoute component={MemberList}></IndexRoute>
        <Route path="/view/:code" component={Member}></Route>
      </Route>
    </Router>
  </Provider>
);
render(route, document.getElementById('root'));
