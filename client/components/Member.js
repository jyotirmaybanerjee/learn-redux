import React, { Component } from 'react';
import Photo from './Photo';
import Details from './Details';

export default class Member extends Component {

  render() {
    const { code } = this.props.params;
    const i = this.props.members.findIndex((member) => member.code === parseInt(code));
    const member = this.props.members[i];
    return (
      <section className="container single-view">
        <div className="photo-block">
          <Photo i={i} member={member} {...this.props} />
        </div>
        <div className="address-block">
          <Details member={member} />
        </div>
      </section>
    );
  }
}
