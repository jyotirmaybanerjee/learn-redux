import React, { Component } from 'react';
import Photo from './Photo';

export default class MemberList extends Component {

  render() {
    return (
      <div className="container bootstrap">
        <div className="main-box no-header clearfix">
          <div className="main-box-body clearfix">
            <div className="table-responsive">
              <table className="table user-list">
                <thead>
                  <tr>
                    <th><span>User</span></th>
                    <th><span>Email</span></th>
                    <th><span>Phone</span></th>
                    <th><span>Mobile</span></th>
                    <th>&nbsp;</th>
                  </tr>
                </thead>

                <tbody>
                  {this.props.members.map((member, i) => (
                    <tr key={i}>
                      <td>
                        <Photo {...this.props} i={i} member={member}/>
                        <a href="#" className="user-link capitalize">{member.name}</a>
                      </td>
                      <td>
                        <a href="#">{member.email}</a>
                      </td>
                      <td>{member.phone}</td>
                      <td>{member.cell}</td>

                      <td style={{width: '20%'}}>
                        <a href={`/view/${member.code}`} className="table-link">
                          <span className="fa-stack">
                            <i className="fa fa-square fa-stack-2x"></i>
                            <i className="fa fa-search-plus fa-stack-1x fa-inverse"></i>
                          </span>
                        </a>
                        <a href={`/view/${member.code}`} className="table-link">
                          <span className="fa-stack">
                            <i className="fa fa-square fa-stack-2x"></i>
                            <i className="fa fa-pencil fa-stack-1x fa-inverse"></i>
                          </span>
                        </a>
                        <a href="#" className="table-link danger" onClick={this.props.removeMember.bind(null, i)}>
                          <span className="fa-stack">
                            <i className="fa fa-square fa-stack-2x"></i>
                            <i className="fa fa-trash-o fa-stack-1x fa-inverse"></i>
                          </span>
                        </a>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
