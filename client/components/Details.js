import React, { PropTypes, Component } from 'react';

export default class Details extends Component {
  constructor(props) {
    super(props);
  }

  render() {

    const { member } = this.props;
    return (
      <div className="details">
        <p><strong>Name: </strong><span className="capitalize">{member.name}</span></p>
        <p><strong>Email: </strong>{member.email}</p>
        <p><strong>Phone: </strong>{member.phone}</p>
        <p><strong>Cell: </strong>{member.cell}</p>
      </div>
    );
  }
}

Details.propTypes = {
};
