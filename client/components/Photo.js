import React, { PropTypes,Component } from 'react';
import { Link } from 'react-router';

export default class Photo extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { member, i } = this.props;
    return (
      <figure>
        <div>
          <Link to={`/view/${member.code}`}>
            <img src={member.picture} alt={member.name} />
          </Link>
        </div>
      </figure>
    );
  }
}

Photo.propTypes = {
};
