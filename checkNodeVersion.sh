NODE_VERSION=$(node -v)
if [[ "${NODE_VERSION%.*}" == v6* ]]; then
  echo "Node version is valid ($NODE_VERSION)"
else
  echo "ERROR: *************** Node v6.x or higher needed for webpack-dashboard. ***************"
  exit 1;
fi