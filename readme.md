Redux
=========

###A sample application to demonstrate redux, react-router, react-router-redux and some use of ES6/ES7.

####Recommended Tools:
  - Google Chrome
  - [Redux DevTools](https://chrome.google.com/webstore/detail/redux-devtools/lmhkpmbekcpmknklioeibfkpmmfibljd?hl=en) (Chrome plugin)

####What all are there

- Basic React ES6 Components
- React router
- React router integration with redux
- React dev-server
- React hot-loading
- Redux implementation
- Source map for development code
- Integrating browser history ( routes) into redux state to enable ***User actions replay*** in ***Redux DevTool*** for easy debugging
- Separating vendor js and application specific js for efficient caching and minimizing page load time
- Versioning build files with file hash to invalidate old cache 
- Generate production html dynamically with the versioned assets inserted
- Compile less to css
- Extract styles from js to separate versioned css file(s)
- React dev server and Express server integration


####Webpack dashboard
- Integrated Webpack dashboard for nice and clean webpack console output (https://github.com/FormidableLabs/webpack-dashboard)

####TODO

- DONE - ~~Separating vendor js and application specific js for efficient caching and minimizing page load time~~
- Add react-form for adding new member(s).
- Implement Backend logic for saving data in DB and fetching data from DB
- Better UI/UX
- Integrate this code base into my existing Dropwizard REST service code base

####How to run

- Clone the repo

```
git clone git@bitbucket.org:jyotirmaybanerjee/learn-redux.git
cd learn-redux
```

- Start dev server

```
npm start
```

- Build
```
npm run build
```

####Note

- To run the dev server you need to use node 6 as Webpack-dashboard requires node 6.

####How to manage multiple node version

- Install nvm

```
npm install nvm
```

- Install a node version 

```
nvm install 6
```

- Toggle between node versions

```
nvm use 6
```

