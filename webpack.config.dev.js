import path from 'path';
import webpack from 'webpack';

module.exports = {
  devtool: 'source-map',
  entry: [
    'webpack-hot-middleware/client',
    './client/members'
  ],
  output: {
    path: path.join(__dirname, 'dist'),
    filename: 'bundle.js',
    publicPath: '/static/'
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin()
  ],
  module: {
    loaders: [
      {
        test: /\.js$/,
        loaders: ['babel'],
        include: path.join(__dirname, 'client')
      },
      {
        test: /\.less$/,
        include: path.join(__dirname, 'client'),
        loader: 'style-loader!css-loader!less-loader'
      },
      {
        test: /\.jpe?g$|\.gif$|\.png|\.png$|\.eot|\.ico|\.svg|\.svg$|\.woff2|\.woff|\.woff|\.ttf|\.ttf/,
        loader: 'file-loader?name=[path][name].[ext]'
      }
    ]
  }
};
